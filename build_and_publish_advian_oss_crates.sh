#!/bin/sh -e

rm -rf "${REPOS}"
mkdir -p "${REPOS}"
export LIBZMQ_INCLUDE_DIR=/usr/include
export LIBZMQ_LIB_DIR=/usr/lib/x86_64-linux-gnu
for repo in rust-datastreamcorelib rust-datastreamservicelib; do
  git clone "https://gitlab.com/advian-oss/${repo}.git" "${REPOS}/${repo}"
  cd "${REPOS}/${repo}"
  cargo publish || true
done
